// libretias
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const fetch = require('node-fetch')
const base64 = require('base-64')

const PAYPAL_API = (env.PAYPAL_SANDBOX === 'TRUE') ? 'https://api-m.sandbox.paypal.com' : 'https://api-m.paypal.com'

const createOrder = async ({ amount, currency, jwt }) => {
  const mode = env.MODE === 'DEV' ? '/dev' : ''
  const urlReturn = env.MODE === 'DEV' ? env.PAGEDEV : env.PAGE
  const body = {
    intent: 'CAPTURE',
    purchase_units: [{
      amount: {
        currency_code: currency,
        value: amount
      }
    }],
    application_context: {
      brand_name: env.NAMEHOST,
      landing_page: 'NO_PREFERENCE', // Default, para mas informacion https://developer.paypal.com/docs/api/orders/v2/#definition-order_application_context
      user_action: 'PAY_NOW', // Accion para que en paypal muestre el monto del pago
      return_url: `${env.HOST}${mode}/api/v2/paypal/execute-payment?jwt=${jwt}&urlReturn=${urlReturn}payment`, // Url despues de realizar el pago
      cancel_url: `${env.HOST}${mode}/api/v2/paypal/cancel-payment?jwt=${jwt}&urlReturn=${urlReturn}payment` // Url despues de realizar el pago
    }
  }
  let response = await fetch(`${PAYPAL_API}/v2/checkout/orders`, {
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${base64.encode(env.PAYPAL_CLIENT_ID + ':' + env.PAYPAL_CLIENT_SECRET)}`
    },
    method: 'POST'
  })
  response = await response.json()
  return response
}
const executePago = async ({ token }) => {
  let response = await fetch(`${PAYPAL_API}/v2/checkout/orders/${token}/capture`, {
    body: '{}',
    headers: {
      'Content-Type': 'application/json',
      authorization: `Basic ${base64.encode(env.PAYPAL_CLIENT_ID + ':' + env.PAYPAL_CLIENT_SECRET)}`
    },
    method: 'POST'
  })
  response = await response.json()
  return response
}

const Paypal = {
  createOrder,
  executePago
}
module.exports = Paypal
