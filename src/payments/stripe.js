// libretias
const Stripe = require('stripe')(process.env.STRIPE_API_KEY_PRIVATE)

/**
 * createCustomer
 * @description create customer with email
 * @param {email}
 * @returns {customer/error}
 */
const createCustomer = async ({ email }) => {
  try {
    const customer = await Stripe.customers.create({
      email
    })
    return customer
  } catch (error) {
    throw new Error(`${error}`)
  }
}
/**
 * getCustomers
 * @description get one customer by id, o get list customer with n items for limit
 * @param {id,limit}
 * @returns {customer/customers/error}
 */
const getCustomers = async ({ id = null, limit = 999999999 }) => {
  if (id) {
    try {
      const customer = await Stripe.customers.retrieve(id)
      return customer
    } catch (error) {
      throw new Error(`${error}`)
    }
  }
  try {
    const customers = await Stripe.customers.list({ limit })
    return customers
  } catch (error) {
    throw new Error(`${error}`)
  }
}

/**
 * getPayments
 * @description get one Paymet by id, o get list Payments with n items for limit
 * @param {id,limit}
 * @returns {payment/payments/error}
 */
const getPayments = async ({ id = null, limit = 999999999 }) => {
  if (id) {
    try {
      const payment = await Stripe.paymentIntents.retrieve(id)
      return payment
    } catch (error) {
      throw new Error(`${error}`)
    }
  }
  try {
    const payments = await Stripe.paymentIntents.list({ limit })
    return payments
  } catch (error) {
    throw new Error(`${error}`)
  }
}

/**
 * createCheckout
 * @description
 * @param {id,limit}
 * @returns {payment/payments/error}
 */
const createCheckout = async ({ paymentMethodTypes, currency, name, unitAmount, quantity, images, successUrl, cancelUrl }) => {
  try {
    const session = await Stripe.checkout.sessions.create({
      payment_method_types: paymentMethodTypes,
      line_items: [
        {
          price_data: {
            currency,
            product_data: {
              name,
              images
            },
            unit_amount: unitAmount
          },
          quantity
        }
      ],
      mode: 'payment',
      success_url: successUrl || `${process.env.HOST}/api/v1/stripe/success`,
      cancel_url: cancelUrl || `${process.env.HOST}/api/v1/stripe/cancel`
    })
    return session
  } catch (error) {
    throw new Error(`${error}`)
  }
}

/**
 * stripe
 * @description object with all methods
 */
const stripe = {
  createCustomer,
  getCustomers,
  getPayments,
  createCheckout
}
module.exports = stripe
