require('module-alias/register')

const checkKeyHost = async (req, res, next) => {
  try {
    const user = req.user__
    const sites = (user.sites || []).map(e => e.host)
    if (!sites.includes(req.query.host)) {
      throw new Error('Host Invalid')
    }
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
  next()
}

module.exports = checkKeyHost
