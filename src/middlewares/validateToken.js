require('module-alias/register')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const fmiddlewares = require('fmiddlewares')

module.exports = fmiddlewares.validateItem({
  token: {
    type: 'compare',
    value: env.TOKEN
  }
}, 'headers')
