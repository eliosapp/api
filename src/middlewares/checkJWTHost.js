require('module-alias/register')
const db = require('@app/db.js')

const checkJWTHost = async (req, res, next) => {
  try {
    const host = req.query.host
    const query = {
      _id: req.jwt__._id
    }
    const respond = await db.get({ query, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Token invalid')
    }
    const user = respond[0]
    const sites = (user.sites || []).map(e => e.host)
    if (!sites.includes(host)) {
      throw new Error('Host Invalid')
    }
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
  next()
}

module.exports = checkJWTHost
