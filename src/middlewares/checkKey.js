require('module-alias/register')
const db = require('@app/db.js')

const checkKey = async (req, res, next) => {
  try {
    const auth = req.headers.authorization
    if (!auth) {
      throw new Error('Key invalid')
    }
    const key = auth.split(' ').pop()
    if (!key) {
      throw new Error('Key invalid')
    }
    const query = {
      key
    }
    const respond = await db.get({ query, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Key invalid')
    }
    req.key__ = key
    req.user__ = respond[0]
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
  next()
}

module.exports = checkKey
