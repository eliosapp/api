require('module-alias/register')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const db = require('@app/db.js')

const checkRole = (role = []) => async (req, res, next) => {
  try {
    const auth = req.headers.authorization
    if (!auth) {
      throw new Error('Token invalid')
    }
    const token = auth.split(' ').pop()
    const result = jwt.verify(token, env.JWT)
    if (!result._id) {
      throw new Error('Token invalid')
    }
    const query = {
      email: result.email,
      uid: result.uid
    }
    const respond = await db.get({ query, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Token invalid')
    }
    const user = respond[0]
    if (!role.includes(user.role)) {
      throw new Error('Not access')
    }
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
  next()
}

module.exports = checkRole
