require('module-alias/register')

module.exports = {
  validateToken: require('@app/middlewares/validateToken'),
  validateJWT: require('@app/middlewares/validateJWT'),
  checkRole: require('@app/middlewares/checkRole'),
  checkJWTHost: require('@app/middlewares/checkJWTHost'),
  checkKey: require('@app/middlewares/checkKey'),
  checkKeyHost: require('@app/middlewares/checkKeyHost')
}
