const io = async (http) => {
  const io = require('socket.io')(http, {
    cors: {
      origin: '*'
    }
  })

  io.on('connection', (socket) => {
    console.log('[SOCKET]:: Connected by =>', socket.id)
  })
}

module.exports = io
