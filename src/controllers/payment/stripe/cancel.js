require('module-alias/register')
/**
 * execute_payment,
 * @description funcion para execute_payment paypal
 * @param {req,res,next}
 */
const cancel = async (req, res, next) => {
  try {
    res.send('Error')
  } catch (error) {
    res.status(401).send({
      type: 'error',
      error: `${error}`
    })
  }
}
module.exports = cancel
