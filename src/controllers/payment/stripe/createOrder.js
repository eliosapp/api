require('module-alias/register')
const stripe = require('@app/payments/stripe')
/**
 * createOrder,
 * @description funcion para crear order para paypal
 * @param {req,res,next}
 */
const createOrderStripe = async (req, res, next) => {
  const data = (req.body || {})
  try {
    const respond = await stripe.createCheckout({
      unit_amount: data.unit_amount,
      currency: data.currency,
      payment_method_types: data.payment_method_types,
      name: data.name,
      quantity: data.quantity,
      images: data.images,
      success_url: data.success_url,
      cancel_url: data.cancel_url
    })
    res.send(respond)
  } catch (error) {
    res.status(401).send({
      type: 'error',
      error: `${error}`
    })
  }
}
module.exports = createOrderStripe
