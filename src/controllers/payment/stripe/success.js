require('module-alias/register')
/**
 * execute_payment,
 * @description funcion para execute_payment paypal
 * @param {req,res,next}
 */
const success = async (req, res, next) => {
  try {
    res.send('OK')
  } catch (error) {
    res.status(401).send({
      type: 'error',
      error: `${error}`
    })
  }
}
module.exports = success
