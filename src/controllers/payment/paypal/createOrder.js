require('module-alias/register')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const db = require('@app/db.js')
const Paypal = require('@app/payments/paypal')
const jwt = require('jsonwebtoken')
/**
 * createOrder,
 * @description funcion para crear order para paypal
 * @param {req,res,next}
 */
const createOrder = async (req, res, next) => {
  const data = (req.body || {})
  const { email, uid } = data
  try {
    const respond = await db.get({ data: { email, uid }, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Data invalid')
    }
    const user = respond[0]
    data.jwt = jwt.sign(user, env.JWT, { expiresIn: '2h' })
    const paypalCheckout = await Paypal.createOrder(data)
    res.send(paypalCheckout)
  } catch (error) {
    res.status(401).send({
      type: 'error',
      error: `${error}`
    })
  }
}
module.exports = createOrder
