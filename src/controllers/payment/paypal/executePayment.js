require('module-alias/register')
const dotenv = require('dotenv').config()
const _jwt = require('jsonwebtoken')
const env = dotenv.parsed
const Paypal = require('@app/payments/paypal')
/**
 * execute_payment,
 * @description funcion para execute_payment paypal
 * @param {req,res,next}
 */
const executePayment = async (req, res, next) => {
  const { token, jwt, urlReturn } = (req.query || {})
  try {
    const user = _jwt.verify(jwt, env.JWT)
    if (!user._id) {
      throw new Error('Token invalid')
    }
    const respond = await Paypal.executePago({ token })
    if (respond.status !== 'COMPLETED') {
      return res.redirect(`${urlReturn}/error`)
    }
    return res.redirect(`${urlReturn}/completed`)
  } catch (error) {
    res.status(401).send({
      type: 'error',
      error: `${error}`
    })
  }
}
module.exports = executePayment
