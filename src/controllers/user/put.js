require('module-alias/register')
const db = require('@app/db.js')

/**
 * postUser,
 * @description funcion para postUser
 * @param {req,res,next}
 */
const updateUser = async (req, res) => {
  try {
    const data = (req.body || {})
    await db.put({
      where: {
        _id: req.jwt__._id
      },
      data: {
        $set: data
      },
      table: 'user'
    })
    res.send({
      type: 'ok',
      msj: 'User update'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = updateUser
