require('module-alias/register')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const { generateKey } = require('@app/functions/index.functions.js')
const db = require('@app/db.js')

/**
 * postUser,
 * @description funcion para postUser
 * @param {req,res,next}
 */
const postUser = async (req, res) => {
  try {
    const data = (req.body || {})
    const query = {
      email: data.email,
      uid: data.uid
    }
    const isExist = await db.get({ query, table: 'user' })
    if (isExist.length === 0) {
      data.key = generateKey(data)
      data.questionnaire = false
      const r = await db.post({ data, table: 'user' })
      if (r.type === 'error') {
        throw new Error(r.error)
      }
    }
    const respond = await db.get({ query, table: 'user' })
    const user = respond[0]
    const token = jwt.sign(user, env.JWT, { expiresIn: '2h' })
    return res.send({
      type: 'ok',
      msj: 'User create',
      token
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = postUser
