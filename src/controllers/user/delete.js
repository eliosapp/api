require('module-alias/register')
const db = require('@app/db.js')

/**
 * deleteUser,
 * @description funcion para deleteUser
 * @param {req,res,next}
 */
const deleteUser = async (req, res) => {
  try {
    const _id = (req.query || {})._id
    await db.delete({
      where: {
        _id
      },
      table: 'user'
    })
    res.send({
      type: 'ok',
      msj: 'User delete'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = deleteUser
