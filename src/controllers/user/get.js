require('module-alias/register')
const db = require('@app/db.js')

/**
 * getUser,
 * @description funcion para getUser
 * @param {req,res,next}
 */
const getUser = async (req, res) => {
  try {
    let query = (req.query || {}).query
    if (!query) {
      query = '{}'
    }
    query = JSON.parse(query)
    const respond = await db.get({ query, table: 'user' })
    return res.send(respond)
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = getUser
