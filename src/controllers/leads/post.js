require('module-alias/register')
const { addClient, useUserAgent } = require('@app/functions/index.functions.js')
const db = require('@app/db.js')

/**
 * postLeads,
 * @description funcion para postLeads
 * @param {req,res,next}
 */
const postLeads = async (req, res) => {
  try {
    const body = (req.body || {})
    const host = req.query.host
    const data = {
      date: (new Date()).getTime(),
      ...body,
      ...useUserAgent(body)
    }
    await db.post({ data, table: `event_${host}` })
    await addClient(data, host)

    return res.send({
      type: 'ok',
      msj: 'Lead save'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = postLeads
