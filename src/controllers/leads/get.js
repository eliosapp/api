require('module-alias/register')
const db = require('@app/db.js')

/**
 * getLeads,
 * @description funcion para getLeads
 * @param {req,res,next}
 */
const getLeads = async (req, res) => {
  try {
    const arg = (req.query || {})
    let query = arg.query
    if (!query) {
      query = '{}'
    }
    query = JSON.parse(query)
    const host = arg.host
    const distinct = arg.distinct
    if (distinct) {
      const result = {}
      const listDistinct = distinct.split(';')
      for (let i = 0; i < listDistinct.length; i++) {
        const element = listDistinct[i]
        result[element] = await db.distinct({ field: element, query, table: `event_${host}` })
      }
      return res.send(result)
    }

    const page = parseInt(arg.page) || 0
    const npage = parseInt(arg.npage) || 10
    let sort = arg.sort || '{ "_id": 1 }'
    sort = JSON.parse(sort)
    const filters = (e) => {
      return e.sort(sort)
        .skip(page > 0 ? ((page - 1) * npage) : 0)
        .limit(npage)
    }
    const respond = await db.get({ query, table: `event_${host}`, preToArray: filters })
    const countLeads = await db.count({ query, table: `event_${host}` })
    return res.send({
      leads: respond,
      countLeads
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = getLeads
