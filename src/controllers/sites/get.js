require('module-alias/register')
const db = require('@app/db.js')

/**
 * getSites,
 * @description funcion para getSites
 * @param {req,res,next}
 */
const getSites = async (req, res) => {
  try {
    const query = {
      _id: req.jwt__._id
    }
    const respond = await db.get({ query, table: 'user' })
    if (!respond) {
      throw new Error('Invalid Token')
    }
    const user = respond[0]
    if (!user) {
      throw new Error('Invalid Token')
    }
    const sites = user.sites || []
    return res.send(sites)
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = getSites
