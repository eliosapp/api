require('module-alias/register')
const db = require('@app/db.js')

/**
 * deleteSites,
 * @description funcion para deleteSites
 * @param {req,res,next}
 */
const deleteSites = async (req, res) => {
  try {
    const query = {
      _id: req.jwt__._id
    }
    const host = req.query.host
    let respond = await db.get({ query, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Token Invalid')
    }
    const user = respond[0]
    const sites = user.sites

    if (!sites.map(e => e.host).includes(host)) {
      throw new Error('Host invalid')
    }

    const newSites = sites.filter((e) => e.host !== host)

    respond = await db.put({
      where: query,
      data: {
        $set: {
          sites: newSites
        }
      },
      table: 'user'
    })
    respond = await db.drop({ query, table: `event_${host}` })
    respond = await db.drop({ query, table: `cliente_${host}` })
    return res.send({
      type: 'ok',
      msj: 'Site delete'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = deleteSites
