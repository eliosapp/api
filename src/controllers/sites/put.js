require('module-alias/register')
const db = require('@app/db.js')

/**
 * putSites,
 * @description funcion para putSites
 * @param {req,res,next}
 */
const putSites = async (req, res) => {
  try {
    const data = (req.body || {})
    const query = {
      _id: req.jwt__._id
    }
    let respond = await db.get({ query, table: 'user' })
    if (!respond) {
      throw new Error('Invalid Token')
    }
    const user = respond[0]
    if (!user) {
      throw new Error('Invalid Token')
    }
    const sites = user.sites || []

    const where = req.query
    if (!sites.map(e => e.host).includes(where.host)) {
      throw new Error('Host invalid')
    }

    const newSites = sites.map(e => {
      if (e.host === where.host) {
        return {
          ...e,
          ...data
        }
      }
      return e
    })
    respond = await db.put({
      where: query,
      data: {
        $set: {
          sites: newSites
        }
      },
      table: 'user'
    })

    return res.send({
      type: 'ok',
      msj: 'Site update'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = putSites
