require('module-alias/register')
const db = require('@app/db.js')

/**
 * postSites,
 * @description funcion para postSites
 * @param {req,res,next}
 */
const postSites = async (req, res) => {
  try {
    const data = (req.body || {})
    const key = req.key__
    const query = {
      key
    }
    const user = req.user__
    const existSite = await db.get({ query: { sites: { $elemMatch: { host: data.host } } }, table: 'user' })
    if (existSite.length > 0) {
      if (existSite[0].key === key) {
        return res.send({
          type: 'ok',
          msj: 'Login ok'
        })
      }
      throw new Error('Sitio ya registrado')
    }
    let sites = (user.sites || []).map(e => e.host)
    if (sites.includes(data.host)) {
      return res.send({
        type: 'ok',
        msj: 'Login ok'
      })
    }
    sites = user.sites || []
    sites.push(data)
    await db.put({
      where: query,
      data: {
        $set: {
          sites
        }
      },
      table: 'user'
    })
    return res.send({
      type: 'ok',
      msj: 'Login ok'
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = postSites
