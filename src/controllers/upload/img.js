require('module-alias/register')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const db = require('@app/db.js')
/**
 * uploadImg,
 * @description funcion para uploadImg
 * @param {req,res,next}
 */
const uploadImg = async (req, res) => {
  try {
    const body = req.body
    const token = body.jwt
    const site = body.site
    const urlReturn = body.return

    const file = req.file
    const filename = file.filename

    const result = jwt.verify(token, env.JWT)
    const query = {
      email: result.email,
      uid: result.uid
    }
    let respond = await db.get({ query, table: 'user' })
    if (!respond) {
      throw new Error('Invalid Token')
    }
    const user = respond[0]
    if (!user) {
      throw new Error('Invalid Token')
    }
    const sites = user.sites || []
    if (!sites.map(e => e.host).includes(site)) {
      throw new Error('Host invalid')
    }
    const newSites = sites.map(e => {
      if (e.host === site) {
        return {
          ...e,
          iconSite: filename
        }
      }
      return e
    })
    respond = await db.put({
      where: query,
      data: {
        $set: {
          sites: newSites
        }
      },
      table: 'user'
    })
    return res.redirect(urlReturn)
  } catch (error) {
    console.log(error)
    return res.status(400).send({ error: error })
  }
}
module.exports = uploadImg
