require('module-alias/register')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv').config()
const env = dotenv.parsed
const db = require('@app/db.js')

/**
 * auth,
 * @description funcion para auth
 * @param {req,res,next}
 */
const auth = async (req, res) => {
  try {
    const query = (req.body || {})
    const respond = await db.get({ query, table: 'user' })
    if (respond.length === 0) {
      throw new Error('Data invalid')
    }
    const user = respond[0]
    const token = jwt.sign(user, env.JWT, { expiresIn: '2h' })
    return res.send({
      token
    })
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      type: 'error',
      error: error,
      msj: `${error}`
    })
  }
}
module.exports = auth
