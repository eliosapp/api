require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateToken = require('@app/middlewares/validateToken')
const auth = require('@app/controllers/auth/auth')

router.post(
  '/',
  [
    validateToken,
    fmiddlewares.validateItem(
      {
        exactItems: true,
        email: {
          type: 'email'
        },
        uid: {
          type: 'string'
        }
      }
    )
  ],
  auth
)

module.exports = router
