require('module-alias/register')

const router = require('express').Router()

router.use('/auth', require('@app/routes/auth.routes'))
router.use('/clients', require('@app/routes/clients.routes'))
router.use('/leads', require('@app/routes/leads.routes'))
router.use('/sites', require('@app/routes/sites.routes'))
router.use('/upload', require('@app/routes/upload.routes'))
router.use('/user', require('@app/routes/user.routes'))
router.use('/paypal', require('@app/routes/paypal.routes'))

module.exports = router
