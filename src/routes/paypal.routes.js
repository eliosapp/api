require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateToken = require('@app/middlewares/validateToken')

const executePayment = require('@app/controllers/payment/paypal/executePayment')
const createOrder = require('@app/controllers/payment/paypal/createOrder')

router.get(
  '/execute-payment',
  [
    fmiddlewares.validateItem({
      token: {
        type: 'string'
      },
      jwt: {
        type: 'string'
      }
    }, 'query')
  ],
  executePayment
)

router.post(
  '/create',
  [
    validateToken,
    fmiddlewares.validateItem({
      exactItems: true,
      amount: {
        type: 'string'
      },
      currency: {
        type: 'string'
      },
      email: {
        type: 'email'
      },
      uid: {
        type: 'string'
      }
    })
  ],
  createOrder
)

module.exports = router
