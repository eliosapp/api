require('module-alias/register')
const router = require('express').Router()

const multer = require('multer')
const upload = multer({ dest: 'src/upload/img/sites' })
const uploadImg = require('@app/controllers/upload/img')

router.post('/', upload.single('photo'), uploadImg)

module.exports = router
