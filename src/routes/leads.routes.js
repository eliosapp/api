require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateJWT = require('@app/middlewares/validateJWT')
const validateToken = require('@app/middlewares/validateToken')
const checkJWTHost = require('@app/middlewares/checkJWTHost')
const checkKey = require('@app/middlewares/checkKey')
const checkKeyHost = require('@app/middlewares/checkKeyHost')

const getLeads = require('@app/controllers/leads/get')
const postLeads = require('@app/controllers/leads/post')

router.get('/', [
  fmiddlewares.validateItem({
    exactItems: true,
    host: {
      type: 'string'
    },
    npage: {
      type: 'string',
      isUndefined: true
    },
    page: {
      type: 'string',
      isUndefined: true
    },
    sort: {
      type: 'string',
      isUndefined: true
    },
    query: {
      type: 'string',
      isUndefined: true
    },
    distinct: {
      type: 'string',
      isUndefined: true
    }
  }, 'query'),
  validateToken,
  validateJWT,
  checkJWTHost
], getLeads)

router.post('/', [
  fmiddlewares.validateItem({
    exactItems: true,
    host: {
      type: 'string'
    }
  }, 'query'),
  checkKey,
  checkKeyHost
], postLeads)

module.exports = router
