require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateJWT = require('@app/middlewares/validateJWT')
const validateToken = require('@app/middlewares/validateToken')
const checkKey = require('@app/middlewares/checkKey')

const getSites = require('@app/controllers/sites/get')
const postSites = require('@app/controllers/sites/post')
const putSites = require('@app/controllers/sites/put')
const deleteSites = require('@app/controllers/sites/delete')
router.get(
  '/',
  [
    validateToken,
    validateJWT
  ],
  getSites
)
router.post(
  '/',
  [
    checkKey,
    fmiddlewares.validateItem({
      exactItems: true,
      host: {
        type: 'string'
      },
      cms: {
        type: 'list',
        list: [
          'wordpress',
          'shopify',
          'woocommerce',
        ]
      },
      icon: {
        type: 'string',
        isUndefined: true
      }
    })
  ],
  postSites
)
router.put('/', [
  validateToken,
  validateJWT,
  fmiddlewares.validateItem({
    exactItems: true,
    host: {
      type: 'string'
    }
  }, 'query'),
  fmiddlewares.validateItem({
    exactItems: true,
    icon: {
      type: 'string'
    }
  })
], putSites)
router.delete('/', [
  validateToken,
  validateJWT,
  fmiddlewares.validateItem({
    host: {
      type: 'string'
    }
  }, 'query')
], deleteSites)

module.exports = router
