require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateJWT = require('@app/middlewares/validateJWT')
const validateToken = require('@app/middlewares/validateToken')
const checkRole = require('@app/middlewares/checkRole')

const getUser = require('@app/controllers/user/get')
const postUser = require('@app/controllers/user/post')
const updateUser = require('@app/controllers/user/put')
const deleteUser = require('@app/controllers/user/delete')

router.get(
  '/',
  [
    validateToken,
    validateJWT,
    checkRole(['admin'])
  ],
  getUser
)
router.post(
  '/',
  [
    validateToken,
    fmiddlewares.validateItem({
      email: {
        type: 'email'
      },
      name: {
        type: 'string',
        isUndefined: true
      },
      surname: {
        type: 'string',
        isUndefined: true
      },
      uid: {
        type: 'string'
      }
    })
  ],
  postUser
)
router.put(
  '/',
  [
    validateToken,
    validateJWT,
    fmiddlewares.validateItem({
      exactItems: true,
      name: {
        type: 'string',
        isUndefined: true
      },
      surname: {
        type: 'string',
        isUndefined: true
      },
      questionnaire: {
        type: 'object',
        isUndefined: true,
        items: {
          exactItems: true,
          I_am: {
            type: 'string'
          },
          n_employees: {
            type: 'string'
          },
          getEmail: {
            type: 'boolean'
          }
        }
      }
    })
  ],
  updateUser
)
router.delete(
  '/',
  [
    validateToken,
    validateJWT,
    checkRole(['admin']),
    fmiddlewares.validateItem({
      exactItems: true,
      _id: {
        type: 'string'
      }
    }, 'query')
  ],
  deleteUser
)

module.exports = router
