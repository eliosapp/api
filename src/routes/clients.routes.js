require('module-alias/register')
const router = require('express').Router()
const fmiddlewares = require('fmiddlewares')
const validateJWT = require('@app/middlewares/validateJWT')
const validateToken = require('@app/middlewares/validateToken')
const checkJWTHost = require('@app/middlewares/checkJWTHost')
const getClients = require('@app/controllers/clients/get')

router.get(
  '/',
  [
    validateToken,
    validateJWT,
    checkJWTHost,
    fmiddlewares.validateItem({
      host: {
        type: 'string'
      }
    }, 'query')
  ],
  getClients
)

module.exports = router
