require('module-alias/register')
const db = require('@app/db.js')

/**
 * addClient,
 * @description funcion para gruarda clientes
 * @param {req,res,next}
 */
const addClient = async (lead, host) => {
  const ip = lead.ip || lead.ipAddress || lead.browser_ip

  const userData = [
    'platform',
    'continentCode',
    'continentName',
    'countryCode',
    'countryName',
    'stateProv',
    'city',
    'os',
    'browser',
    'system'
  ]
  const update = {}
  userData.forEach(key => {
    if (lead[key]) {
      update[key] = lead[key]
    }
  })
  const compras = (lead.type === 'orders-paid') ? 1 : 0
  const form = ((lead.event || {}).type === 'Form Submit') ? 1 : 0
  const whatsapp = ((lead.event || {}).type === 'click' && (lead.event || {}).class === 'joinchat__button__sendtext') ? 1 : 0
  const respond = await db.put({
    where: {
      ip
    },
    data: {
      $set: {
        ip,
        ...update
      },
      $inc: {
        leads: 1,
        compras,
        form,
        whatsapp
      },
      $addToSet: {
        gets: lead.get
      }
    },
    options: {
      upsert: true
    },
    table: `cliente_${host}`
  })
  return respond
}
module.exports = addClient
