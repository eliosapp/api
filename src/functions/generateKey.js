const dotenv = require('dotenv').config()
const env = dotenv.parsed

const generateKey = (json) => {
  return (Buffer.from((json.email + json.uid).split('').reverse().join(env.KEYJOIN + Math.random()))).toString('base64').replace(/[|+=*&^%$#@!~\-(){}[\];:'"/\\><]/g, '')
}
module.exports = generateKey
