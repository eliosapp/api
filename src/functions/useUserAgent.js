const useUserAgent = (data) => {
  const json = {}
  if (data.userAgent !== undefined) {
    const userAgent = data.userAgent.toLocaleLowerCase()
    const OS = {
      mac: 'Mac OS X',
      ipad: 'iPad',
      ipod: 'iPod',
      iphone: 'iPhone',
      imac: 'Mac',
      android: 'Android',
      linux: 'Linux',
      nokia: 'Nokia',
      blackberry: 'BlackBerry',
      win: {
        'nt 10.0': 'Windows 10',
        'nt 6.2': 'Windows 8',
        'nt 6.3': 'Windows 8.1',
        'nt 6.1': 'Windows 7',
        'nt 6.0': 'Windows Vista',
        'nt 5.1': 'Windows XP',
        'nt 5.0': 'Windows 2000',
        default: 'Windows'

      },
      freebsd: 'FreeBSD',
      openbsd: 'OpenBSD',
      netbsd: 'NetBSD',
      opensolaris: 'OpenSolaris',
      'os/2': 'OS/2',
      beos: 'BeOS'
    }
    json.os = 'Unknown'

    for (const i in Object.keys(OS)) {
      const key = Object.keys(OS)[i]
      if (userAgent.indexOf(key) > -1) {
        let value = OS[key]
        if (typeof value === 'object') {
          const OS2 = value
          for (const j in Object.keys(OS2)) {
            const key2 = Object.keys(OS2)[j]
            if (userAgent.indexOf(key2) > -1) {
              value = value[key2]
              break
            }
          }
          if (typeof value === 'object') {
            value = value.default
          }
        }
        json.os = value
        break
      }
    }

    const browser = {
      edge: 'Edge',
      msie: 'Explorer',
      firefox: 'Firefox',
      chrome: 'Chrome',
      safari: 'Safari',
      opera: 'Opera',
      outlook: 'Outlook'
    }
    json.browser = 'Unknown'
    for (const i in Object.keys(browser)) {
      const key = Object.keys(browser)[i]
      if (userAgent.indexOf(key) > -1) {
        const value = browser[key]
        json.browser = value
        break
      }
    }
    json.system = '32bit'
    if (userAgent.indexOf('x64') > -1) json.system = '64bit'
    if (userAgent.indexOf('WOW64') > -1) json.system = '64bit'
    if (userAgent.indexOf('x86_64') > -1) json.system = '64bit'
  }
  return json
}
module.exports = useUserAgent
