require('module-alias/register')

exports.generateKey = require('@app/functions/generateKey')
exports.addClient = require('@app/functions/addClient')
exports.useUserAgent = require('@app/functions/useUserAgent')
